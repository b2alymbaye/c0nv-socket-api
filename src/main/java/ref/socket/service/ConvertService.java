package ref.socket.service;

import com.google.common.base.Preconditions;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;

import java.net.InetAddress;
import java.net.UnknownHostException;

@Service
public class ConvertService {
    private final String URL_EXPRESSION = "(https?:\\/\\/(?:www\\.|(?!www))[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\\.[^\\s]{2,}|www\\.[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\\.[^\\s]{2,}|https?:\\/\\/(?:www\\.|(?!www))[a-zA-Z0-9]+\\.[^\\s]{2,}|www\\.[a-zA-Z0-9]+\\.[^\\s]{2,})";
    private final String IP_EXPRESSION = "\\b(?:[0-9]{1,3}\\.){3}[0-9]{1,3}\\b";


    /**
     * Convert url address to ip.
     *
     * @param url url
     * @return ip
     */
    public String convertUrlToIp(String url) {
        Preconditions.checkArgument(StringUtils.isNotBlank(url), "L'url est obligatoire.");
        if(!url.matches(URL_EXPRESSION)){ // Check url format
            return "Erreur : Le format de l'url n'est pas bon.";
        }

        String ip = "";
        try {
            ip = InetAddress.getByName(url).getHostAddress();
        } catch (UnknownHostException e) {
            return "Erreur : Impossible de trouver une correspondance pour l'url " + url;
        }

        return ip;
    }

    /**
     * Convert ip to url.
     *
     * @param ip ip  // IPV4
     * @return usr
     */
    public String convertIpToUrl(String ip) {
        Preconditions.checkArgument(StringUtils.isNotBlank(ip), "L'ip est obligatoire.");
        if(!ip.matches(IP_EXPRESSION)){ // Check ip format
            return "Erreur : Le format de l'ip n'est pas bon.";
        }

        String url = "";
        try {
            url = InetAddress.getByName(ip).getHostName();
        } catch (UnknownHostException e) {
            return "Erreur : Impossible de trouver une correspondance pour l'ip " + ip;
        }

        return url;
    }
}
