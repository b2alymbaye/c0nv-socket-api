package ref.socket.controller;

import com.google.common.base.Preconditions;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ref.socket.service.ConvertService;

@RestController
@RequestMapping("/api/convert")
public class ConvertController {
    private ConvertService convertService;

    @Autowired
    public ConvertController(ConvertService convertService) {
        this.convertService = convertService;
    }

    @GetMapping(path = "/url/{url}")
    public String convertUrlToIp(@PathVariable String url){
        Preconditions.checkArgument(StringUtils.isNotBlank(url), "L'url est obligatoire.");
        return convertService.convertUrlToIp(url);
    }


    @GetMapping(path = "/ip/{ip}")
    public String convertIpToUrl(@PathVariable String ip){
        Preconditions.checkArgument(StringUtils.isNotBlank(ip), "L'ip est obligatoire.");
        return convertService.convertIpToUrl(ip);
    }
}
